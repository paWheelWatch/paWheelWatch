function [angles] = angleCalculation(cleanedTransPoints)

%Initialisierung

angles=zeros((size(cleanedTransPoints,1)/7),3);

j=1;
for i=1:7:size(cleanedTransPoints,1)
    angles(j,1)=cleanedTransPoints(i,1);
    midPoint=(cleanedTransPoints(i+1,2:4)+cleanedTransPoints(i+2,2:4))/2;
    n=cross(cleanedTransPoints(i,2:4)-midPoint,cleanedTransPoints(i+1,2:4)-midPoint); %normale
    
    if cleanedTransPoints(i+1,1)-cleanedTransPoints(i+2,1)>0
        angles(j,2)=-acosd([n(1),n(2),0]/norm([n(1),n(2),0])*[1;0;0]);  %Breitenwinkel/ x drehung
    else
        angles(j,2)=acosd([n(1),n(2),0]/norm([n(1),n(2),0])*[1;0;0]);  %Breitenwinkel/ x drehung
    end
    
    %angles(i,2)=acosd(n/norm(n)*[0;1;0]); % beta y Drehung

    angles(j,3)= 90 - acosd(n/norm(n)*[0;0;1]); %H�henwinkel/ 90-z drehung
    angles(j,3)= 90 - acosd(n/norm(n)*[0;0;1]); %H�henwinkel/ 90-z drehung
    j=j+1;
end
%%PROJEKTION!%%

end