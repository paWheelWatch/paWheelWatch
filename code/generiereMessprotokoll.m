function generiereMessprotokoll(File,Path,Institut,Projektnummer,Kunde,Fahrzeug,DatumMessung,Namenskuerzel,Ort,Table1,Table2,Umfang, FL, FR, RL, RR)
    id=Table1(:,1);
    LoadingBar = waitbar(0,'Bitte Warten...');
    Nummer=cellstr(num2str(id));
    CGX=cellstr(num2str(round(Table1(:,2),1)));
    CGY=cellstr(num2str(round(Table1(:,3),1)));
    CGZ=cellstr(num2str(round(Table1(:,4),1)));
    SENSX=cellstr(num2str(round(Table1(:,5),1)));
    SENSY=cellstr(num2str(round(Table1(:,6),1)));
    SENSZ=cellstr(num2str(round(Table1(:,7),1)));
    YAW=cellstr(num2str(round(Table1(:,8),3)));
    PITCH=cellstr(num2str(round(Table1(:,9),3)));
    ROLL=cellstr(num2str(round(Table1(:,10),3)));
    n=size(Nummer);
    
    fullname=Table2(:,1);
    fullid=cell2mat(Table2(:,2));
    fulln=size(fullid);
    
    for i = 1:n
        for j = 1:fulln
            if id(i)==fullid(j)
                NAME(i)=fullname(j);
                break
            end
        end
    end
    NAME = reshape(NAME,n);        
    
	FileSpec = fullfile(Path,File);
% Start an ActiveX session with Word:
    word = actxserver('Word.Application');      %start Word
    word.Visible =0;                            %make Word Visible
    if ~exist(FileSpec,'file')
        % Create new document:
        document = invoke(word.Documents,'Add');
    else
        % Open existing document:
        document = invoke(word.Documents,'Open',FileSpec);
        word.Selection.WholeStory;
        word.Selection.Delete;
    end    
    
    %document=word.Documents.Add;               %create new Document
    selection=word.Selection;                   %set Cursor
    selection.Font.Name='Arial';                %set Font
    selection.Font.Size=9;                      %set Size

    selection.Pagesetup.RightMargin=28.34646;   %set right Margin to 1cm
    selection.Pagesetup.LeftMargin=28.34646;    %set left Margin to 1cm
    selection.Pagesetup.TopMargin=28.34646;     %set top Margin to 1cm
    selection.Pagesetup.BottomMargin=28.34646;  %set bottom Margin to 1cm
                                                %1cm is circa 28.34646 points
    selection.Paragraphs.LineUnitAfter=0.01;    %sets the amount of spacing 
                                                %between paragraphs(in gridlines)
    
                                                
    waitbar(0.1);
    
    TextString='Messprotokoll - paWheelWatch';
    heading(selection,TextString,18);
    selection.TypeParagraph; 
       
    TextString='Allgemeine Informationen';
    heading(selection,TextString,12);
    
    DataCell={'Institut', Institut;
              'Projektnummer', num2str(Projektnummer);
              'Kunde', Kunde;
              'Fahrzeug', Fahrzeug;
              'Datum der Messung', DatumMessung;
              'Namenskürzel', Namenskuerzel;
              'Ort der Messung', Ort;
              'Datum der Erstellung des Protokolls', date};
                   
    %create table with data from DataCell
    WordCreateTable(word,DataCell);
    
    selection.MoveUp;
    selection.MoveRight(1,1,0);
    selection.InsertColumnsRight;
    selection.Cells.Merge;
    selection.MoveLeft(1,1,0);
    if (Institut == 'ika')
        selection.InlineShapes.AddPicture([pwd '/ika_logo.jpg'],0,1);
    elseif(Institut == 'fka')
        selection.InlineShapes.AddPicture([pwd '/fka_logo.jpg'],0,1);     
    end
    selection.MoveDown;
    selection.TypeParagraph; 
    selection.TypeParagraph;
    
    waitbar(0.3);
    
    TextString='Begriffserklärungen';
    heading(selection,TextString,12); 
    selection.Font.Name='Arial';
    selection.Font.Size=9;  
    selection.ParagraphFormat.Alignment =0;
    selection.TypeText('CGX/CGY/CGZ: Positionen der Sensorenschwerpunkte)');
    selection.TypeParagraph;
    selection.TypeText('SENSX/SENSY/SENSZ: Positionen der Sensorenmesspunkte');
    selection.TypeParagraph;
    selection.TypeText('YAW: Drehung des Sensors um die z-Achse');
    selection.TypeParagraph;
    selection.TypeText('PITCH: Drehung des Sensors um die y-Achse');
    selection.TypeParagraph;
    selection.TypeText('ROLL: Drehung des Sensors um die x-Achse');
    selection.TypeParagraph;
    selection.TypeParagraph; 
    selection.TypeParagraph; 
    
    waitbar(0.4);
    
    %
    % gesamte Tabelle
    %
    TextString='Sensorendaten';
    heading(selection,TextString,12);
    
    DataCell={'Sensorname','Sensor ID', 'CGX', 'CGY', 'CGZ', 'SENSX', 'SENSY', 'SENSZ', 'YAW', 'PITCH', 'ROLL'};
    DataCell(2:(n+1),1)=NAME;
    DataCell(2:(n+1),2)=Nummer;
    DataCell(2:(n+1),3)=CGX;
    DataCell(2:(n+1),4)=CGY;
    DataCell(2:(n+1),5)=CGZ;
    DataCell(2:(n+1),6)=SENSX;
    DataCell(2:(n+1),7)=SENSY;
    DataCell(2:(n+1),8)=SENSZ;
    DataCell(2:(n+1),9)=YAW;
    DataCell(2:(n+1),10)=PITCH;
    DataCell(2:(n+1),11)=ROLL;          
    WordCreateTable(word,DataCell);
    selection.TypeParagraph; 
    selection.TypeParagraph; 
    
    TextString='Hinweise';
    heading(selection,TextString,12);
    selection.TypeText('Sämtliche nachfolgenden Grafiken dienen nur zur Veranschaulichung. Die tatsächliche Sensorenposition bzw. das Aussehen des tatsächlichen Fahrzeugs können abweichen.');
    selection.TypeParagraph; 
    selection.TypeParagraph;
    
    TextString='Figure Back';
    heading(selection,TextString,12);
    selection.InlineShapes.AddPicture([pwd '/figRear.png'],0,1);
    
    selection.InsertNewPage;
     
    TextString=['Figure Top'];
    heading(selection,TextString,12);
    selection.InlineShapes.AddPicture([pwd '/figTop.png'],0,1);
    
    
    waitbar(0.6);
    if(strcmp(Umfang,'gesamt'))
        selection.InsertNewPage;
        TextString=['Auflistung der einzelnen Sensoren'];
        heading(selection,TextString,16);
        selection.TypeParagraph; 
        
        for i=1:n
            if and((mod(i,3)==1),i>1)
                selection.InsertNewPage;
            end
            TextString=['Sensor: ' char(NAME(i))];
            heading(selection,TextString,12);
            
            %create figures
            sensToPlot = zeros(3,10);
            sensToPlot(3,:) = Table1(i,:);
            figTemp = fcn_PlotSensorRear(sensToPlot, FL, FR, RL, RR);
            legend('off')          
            set(findobj(figTemp, 'Type', 'line'), 'linewidth', 3);
            set(findobj(figTemp, 'Type', 'line'), 'MarkerSize', 24);
            set(findobj(figTemp, 'Type', 'line'), 'Color', [1 0 0]);
            saveas(figTemp, './figTempRear.png');
            I = imread('./figTempRear.png');
            J = imresize(I,[250 NaN]);
            imwrite(J, './figTempRear.png');
            
            figTemp = fcn_PlotSensorTop(sensToPlot, FL, FR, RL, RR);
            legend('off')
            set(findobj(figTemp, 'Type', 'line'), 'linewidth', 3);
            set(findobj(figTemp, 'Type', 'line'), 'MarkerSize', 24);
            set(findobj(figTemp, 'Type', 'line'), 'Color', [1 0 0]);
            saveas(figTemp, './figTempTop.png');
            I = imread('./figTempTop.png');
            J = imresize(I,[250 NaN]);
            imwrite(J, './figTempTop.png');
            
            DataCell=   {'Sensor ID', cell2mat(Nummer(i));
                        'CGX', cell2mat(CGX(i));
                        'CGY', cell2mat(CGY(i));
                        'CGZ', cell2mat(CGZ(i));
                        'SENSX', cell2mat(SENSX(i));
                        'SENSY', cell2mat(SENSY(i));
                        'SENSZ', cell2mat(SENSZ(i));
                        'YAW', cell2mat(YAW(i));
                        'PITCH', cell2mat(PITCH(i));
                        'ROLL', cell2mat(ROLL(i))};
            WordCreateTable(word,DataCell);
            selection.MoveUp;
            selection.MoveRight(1,1,0);
            selection.InsertColumnsRight;
            selection.Cells.Merge;
            selection.MoveLeft(1,1,0);
            selection.InlineShapes.AddPicture([pwd '/figTempTop.png'],0,1);
            selection.InsertColumnsRight;
            selection.Cells.Merge;
            selection.MoveLeft(1,1,0);
            selection.InlineShapes.AddPicture([pwd '/figTempRear.png'],0,1);
            selection.MoveDown;
            
            selection.TypeParagraph; 
            selection.TypeParagraph; 
        end
    end
   
    CloseWord(word,document,FileSpec);    
    close all;
return

function WordCreateTable(wordHandle,dataCell)
    [NoRows,NoCols]=size(dataCell);
    wordHandle.ActiveDocument.Tables.Add(wordHandle.Selection.Range,NoRows,NoCols,1,1);
    for r=1:NoRows
        for c=1:NoCols
            wordHandle.Selection.TypeText(dataCell{r,c});
            if((nargin==3)&&(r==1))
               wordHandle.Selection.Shading.BackgroundPatternColor = -603937025;
            end
            if(r*c==NoRows*NoCols)
                wordHandle.Selection.MoveDown;
            else
                wordHandle.Selection.MoveRight;
            end            
        end
    end
return

function CloseWord(actx_word_p,word_handle_p,word_file_p)
    if ~exist(word_file_p,'file')
        invoke(word_handle_p,'SaveAs',word_file_p,1);
    else
        invoke(word_handle_p,'Save');
    end
    invoke(word_handle_p,'Close');            
    invoke(actx_word_p,'Quit');            
    delete(actx_word_p);            
return

function heading(cursor,textString, size)
	cursor.TypeText(textString);         %write Text
    cursor.TypeParagraph;                %line break
    cursor.MoveUp(5,1,1);                %mark previous row
    cursor.Font.Bold=1;                  %set text to Bold
    cursor.Font.Size=size;               %set Size
    cursor.Font.Underline=1;             %change underline
    cursor.MoveDown(5,1);                %move cursor back down
return