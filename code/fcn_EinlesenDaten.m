function[RawInput] = fcn_EinlesenDaten()

%% Funktionsbeschreibung
%Einlesen der Daten aus dem xls mit Namen DateiName####

%% Code
[Name, Pfad] = uigetfile('.xls','Waehlen Sie einen Datensatz aus');

if any(Name) ~= 0 && any(Pfad) ~= 0
    RawInput = xlsread([Pfad, Name]);
else
%Wenn die Benutzer uigetfile abbrechen
    RawInput = 0;
end


%Uebergebener Dateiname (bereits String) wird mittels xlsread Kommando importiert.
%Rueckgabe ist eine Matrix mit den Argumenten aus dem Excel sheet.
%Ueberschriften und Tabellenbenennung werden ignoriert.
%Eintraege die keine Zahl sind werden mit n.a. ausgefiltert
end
