%###########NUR RAW INPUT##############################
%###########NUR RAW INPUT##############################
%###########NUR RAW INPUT##############################
%gibt einen bestimmten Marker(0-6) und die Matrix mit xyz zurueck
function[Marker] = PunktAusgabe (Number,RawInput)

%routine zum erkennen wenn wert nicht vorhanden muss eingefuegt werden
Intern = Number * 10;
j = 1;
MarkerPos = 0;
Marker=zeros(7,4);
sizeRawInput = size(RawInput);
sizeY=sizeRawInput(1,1);


for i=0:sizeY(1,1)
    if RawInput(j,2) == Intern
        MarkerPos = j;
        i = sizeY(1,1);
    else
        j = j+1;
    end
    
    
end
for x=1:7
    Marker(x,:) = [RawInput(MarkerPos,2) RawInput(MarkerPos,4) RawInput(MarkerPos,5) RawInput(MarkerPos,6)];
    MarkerPos = MarkerPos+1;
end

end