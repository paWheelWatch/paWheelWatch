%###########NUR RAW INPUT##############################
%###########NUR RAW INPUT##############################
%###########NUR RAW INPUT##############################
function [Wert] = fcn_sub_LastPoint (RawInput)
%erfassen letzter sinvoller Eintrag Tabelle evtl Funktions eingabe
%erweitert mit maxMarker!!!
maxMarker = 1100;
sizeRawInput = size(RawInput);
sizeX = sizeRawInput(1,1);
lastPoint = sizeX;
for i = sizeX:-1:0
    j = RawInput(lastPoint,2);
    
    if j > maxMarker
        lastPoint = lastPoint-1;
    else
        sizeX = 0;
    end
    
    
    
end
Wert = RawInput(lastPoint,2);
end