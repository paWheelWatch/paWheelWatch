function [vEnd] = drawTran(vStart,length,Rp,prevy,prevz)
istart = 1;
iend = 100;
jstart = 1;
jend = 100;
lv = norm(vStart);
u = vStart / lv;
del = length - lv;
v = vStart;
an1 = animatedline('Marker', '*');
an2 = animatedline('Color', 'r');
for i=istart:iend
    if i > istart
        clearpoints(an1)
        clearpoints(an2)
    end
    v = vStart + ((i - istart)/(iend - istart)) * del * u;
    addpoints(an1, v(1), v(2), v(3));
    for j=jstart:jend
        phi = (j - jstart)/(jend - jstart);
        addpoints(an2, phi * v(1), phi * v(2), phi * v(3));
    end
    drawnow limitrate
    [X, Y, Z] = genPlane([v(1), v(2), v(3)]', [v(1), v(2), v(3)], Rp,prevy,prevz);
    p = patch(X,Y,Z,[1 1 1 1]);
    pause('on')
    pause(0.001);
    if i ~= iend
        delete(p);
    end
end
vEnd = v;
end

