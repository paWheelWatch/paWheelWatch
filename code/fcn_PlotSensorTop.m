function [fig] = fcn_PlotSensorTop (SensorInput, FL, FR, RL, RR)
%% Initialisierung
%Erstellen eines TopPlots mit Hintergrund
%Scaling lasst sich einstellen, damit die Nullpunkte der Achsen auch mit
%dem Bild uebereinstimmen. Siehe Abschnitt Scaling.

Punkte = SensorInput;
fig = figure('Name','Sensoren Top View','position' , [200, 200, 400, 800], 'visible','off');

%% 3D
fv = stlread('2011 VW Passat .stl');
% workaround fuer das passat modell
fmidx =  (max(fv.vertices(:,1)) + min(fv.vertices(:,1))) / 2;
fmidy =  (max(fv.vertices(:,2)) + min(fv.vertices(:,2))) / 2;
fmidz =  (max(fv.vertices(:,3)) + min(fv.vertices(:,3))) / 2;
for i=1:size(fv.vertices, 1)
    if fv.vertices(i,3) <= 0
        fv.vertices(i,:) = [fmidx fmidy fmidz];
    end
end
% scaling = wheelbase / length, see passat-*-dimensions.pdf
scaling = 1.71;
upperB = size(Punkte,1);
% NullPunkt verschieben auf Mitterpunkt der HA, fuer y muss das leider fahrzeugspezifisch definiert werden
fv.vertices = fv.vertices - [fmidx 0.9*scaling*fmidy fmidz];
% % rotation -90 Grad um z Achse, also Fahrzeugachse -> Modellachse
% for i = 3:upperB
%     Punkte(i,2:4) = (genRotMat('z', -90) * Punkte(i,2:4)')';
%     Punkte(i,2) = -Punkte(i,2);
% end
tempvertices = genRotMat('z',90) * fv.vertices';
fv.vertices = tempvertices';

patch(fv,'FaceColor',[0.8 0.8 1.0],'EdgeColor','none','FaceLighting','gouraud','AmbientStrength',0.15);
fdx = max(fv.vertices(:,1)) - min(fv.vertices(:,1));
fdy = max(fv.vertices(:,2)) - min(fv.vertices(:,2));
fdz = max(fv.vertices(:,3)) - min(fv.vertices(:,3));

camlight('headlight');
material('dull');
axis('image');
%axis ij;
view([-90 90]); % Top
%alpha (0.5);
if upperB == 0
    return
end

hold all
% achten drauf dass das Modell ein unterschiedliches Koord-System hat
% Fahrzeug y 
rangey = (norm(FR - FL) + norm(RL - RR)) / 2;
% Fahrzeug x 
rangex = scaling * (norm(FR - RR) + norm(FL - RL)) / 2;
factorx = fdx / rangex;
factory = fdy / rangey;
% height = 1477 mm, see passat-*-dimensions.pdf
factorz = fdz / 1477;
for i=3:upperB
    x=factorx * (Punkte(i,2));
    y=factory * (Punkte(i,3));
    % in den Vordergrund der Draufsicht ziehen
    z=factorz * (Punkte(i,4)) + 2 * fdz;
    plot3(x,y,z,'*');
end
blank=' ';
threeD = '3D';
if length(num2str(max(Punkte(3:upperB,1)))) > 2
    for i = 1:length(num2str(max(Punkte(3:upperB,1)))) - 2
        threeD = [blank threeD];
    end
end
legend([threeD;num2str(Punkte(3:upperB,1))]);
hold off
end