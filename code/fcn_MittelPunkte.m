%% MittelPunkte
function [MittelPunkte] = fcn_MittelPunkte(RawInput)

%Sortiert den RawInput sodass nur MittelPunkte als Ausgabe enstehen
%Wichtig dabei nur RAWINPUT kann verarbeitet werden
%###########NUR RAW INPUT##############################
%###########NUR RAW INPUT##############################
%###########NUR RAW INPUT##############################

%% Initialisierung

PosLastPoint = fcn_PositionLastPoint(RawInput);
%Abfrage des letzten Punktes Aus dem RawInput um die maximale Grsssse des
%Outputs zu bestimmen

Zaehler = 1;
%Schleifenzaehler fuer MittelPunkte

%% Schleife zum Filtern
for i = 1:PosLastPoint
    rest = mod(RawInput(i,2),10);
    %gesucht wird die Marker Nummer die ohne Rest durch 10 Teilbar ist.
    %Die Marker Punkte sind nach dem Schema XXX0 fuer den grossen MittelPunkt
    %aufgebaut.
    %Damit werden die MittelPunkte der Marker gefunden
    if rest == 0
        MittelPunkte(Zaehler,:) = [RawInput(i,2),RawInput(i,4),RawInput(i,5),RawInput(i,6)]; %[Nr, x, y, z]
        %Hier werden die Daten des Punktes in die neue Matrix geschreiben
        
        Zaehler = Zaehler+1;
        %index der OutputMatrix wird angepasst nach jeder Iteration   
    end
    %If prueft ob der aktuelle Punkt ein MittelPunkt ist, da dann rest = 0.
end
end