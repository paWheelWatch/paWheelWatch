function [Rot] = winkel2quad(zeta, gamma, centaury)
%Winkel in RAD

RotAngZeta = 2*acos(zeta);
RotAngGamma = 2*acos(gamma);
RotAngCentaury = 2*acos(centaury);

cy = cos(zeta/2);
sy = sin(zeta/2);
cr = cos(gamma/2);
sr = sin(gamma/2);
cp = cos(centaury/2);
sp = sin(centaury/2);

q0 = cy * cr * cp + sy * sr * sp;
q1 = cy * sr * cp - sy * cr * sp;
q2 = cy * cr * sp + sy * sr * cp;
q3 = sy * cr * cp - cy * sr * sp;

Rot = [q0^2+q1^2-q2^2-q3^2, 2*(q1*q2-q0*q3),2*(q0*q2+q1*q3);...
    2*(q1*q2+q0*q3), q0^2-q1^2+q2^2-q3^2,2*(q2*q3-q0*q3);...
    2*(q1*q3-q0*q2),2*(q0*q1+q2*q3),q0^2-q1^2-q2^2+q3^2];


end