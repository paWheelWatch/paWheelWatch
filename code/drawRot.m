function [vEnd, Rp] = drawRot(R,vStart,nRotAnima,nLineAnima,rotAn, lineAn, lastOp, Rp, prevy, prevz)
% rotation matrix, start vector, rotation speed, #points on a line,
% animatedline for rotation, animatedline for line
global Pitch Yaw
dprevy = 0;
dprevz = 0;
v = vStart;
istart = 1;
iend = nRotAnima;
jstart = 1;
jend = nLineAnima;
if prevy == 0 && prevz == 0
    dprevy = Pitch / iend;
elseif prevy ~= 0 && prevz == 0
    dprevz = Yaw / iend;
end
for i=istart:iend
    %view(v);
    vold = v;
    v = R * v;
    if i ~= istart && all(vold == v)
        % rotate the plane instead
        Rp = Rp * R;
    else
        if i > istart
            clearpoints(rotAn)
            clearpoints(lineAn)
        end
        for j=jstart:jend
            % transformation into [0 1]
            phi = ((j - jstart) / (jend - jstart));
            if phi ~= 1
                addpoints(lineAn, phi * v(1), phi * v(2), phi * v(3));
            else
                addpoints(rotAn, v(1), v(2), v(3));
            end
        end
        drawnow limitrate
        prevy = prevy + dprevy;
        prevz = prevz + dprevz;
    end
    [X, Y, Z] = genPlane([v(1), v(2), v(3)]', [v(1), v(2), v(3)], Rp, prevy, prevz);
    p = patch(X,Y,Z,[1 1 1 1]);
    pause('on')
    pause(0.001);
    if i ~= iend || lastOp == 0
        delete(p);
    end
end
vEnd = v;
end

