function [PositionLastPoint] = fcn_PositionLastPoint (RawInput)
%Ausgabe ist der letzte Punkt des Raw Inputs (Markernummer)
%###########NUR RAW INPUT##############################
%###########NUR RAW INPUT##############################
%###########NUR RAW INPUT##############################
%%#################ACHTUNG!!!
%%NICHT DAS GLEICHE WIE LastPoint
%%Nur die Position des lastPoint wird abgefragt!!!!

%% Initialisierung

maxMarker = 40000;
%Damit Punkte >50000 rausgefiltert werden

sizeRawInput = size(RawInput,1);

%Groesse der Schleife festlegen
lastPoint = sizeRawInput;

%% Schleife zum rausfiltern der Stuetzpunkte die durch Aicon gesetzt werden

for i = sizeRawInput:-1:0
    if RawInput(lastPoint,2) > maxMarker
        lastPoint = lastPoint-1;
    else
        break;
    end
end
%Schleife Zaehlt rueckwaerts!

PositionLastPoint = RawInput(lastPoint,1);
%Ausgabe ist der letzte Punkt des Raw Inputs (Markernummer)

end