function [X,Y,Z] = genPlane(origin,normal,R,prevy,prevz)
    w = null(normal); % Find two orthonormal vectors which are orthogonal to v
%     [P,Q] = meshgrid(-1:1); % Provide a gridwork (you choose the size)
%     X = origin(1)+w(1,1)*P+w(1,2)*Q; % Compute the corresponding cartesian coordinates
%     Y = origin(2)+w(2,1)*P+w(2,2)*Q; %   using the two vectors in w
%     Z = origin(3)+w(3,1)*P+w(3,2)*Q;
%     surf(X,Y,Z);
    sf = 0.1;
    XYZ = [origin + sf * w(:,1) + sf * w(:,2) origin + sf * w(:,1) - sf * w(:,2) origin - sf * w(:,1) - sf * w(:,2) origin - sf * w(:,1) + sf * w(:,2)];
    XYZ = genRotMat('z',prevz) * genRotMat('y',prevy) * R * genRotMat('y',-prevy) * genRotMat('z',-prevz) * XYZ;
    
    X = XYZ(1,:);
    Y = XYZ(2,:);
    Z = XYZ(3,:);
end

