function [CleanInput] = fcn_Bereinigen (RawInput)
%% Initialisierung
PosLastPoint = fcn_PositionLastPoint(RawInput);
Intern = 1;
SizeAllPoints = PosLastPoint;
%wie viele Eintraege haben alle PUNKTE
CleanInput = zeros(SizeAllPoints,4);
% Speicherbelegung
for x=1:SizeAllPoints
    CleanInput(x,:) = [RawInput(Intern,2) RawInput(Intern,4) RawInput(Intern,5) RawInput(Intern,6)];
    Intern = Intern +1;
end
%Raussortieren der unnoetigen Datensaetze und als Output weiterreichen
end