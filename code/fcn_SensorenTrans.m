function [SensorMatrixTrans] = fcn_SensorenTrans (SensorOffset, CleanInputTrans)
%% Initialisierung
%%#######Braucht Clean Input bereits Transformiert!!!######
%Ausgabe: SensorMatrixTrans
%Wichtig sind die Punkte 1 und 2 mit denen sich ein weiterer Punkt und
%damit eine richtung des Markers definieren lsst

OhneRaeder = 8;
%Eintrag 8 im VM damit ausgabe ohne Rder und Kreuz Punkte erfolgt

SensorOffsetSize = size(SensorOffset,1);
SensorMatrixTrans = zeros(SensorOffsetSize-OhneRaeder,10);
% SensorMatrix7 = PunktAusgabeClean(SensorNummer,CleanInputTrans);

SensorMatrix3 = zeros(3,3);
SensorOffsetHilfe = zeros(1,7);
%CGOffset = zeros(1,3);%Matlab sagt unntig weil bereist allocated wird???
%SENSOffset = zeros(1,3);%siehe oben
%^--- Unklar evtl. doch hilfreich?

%% Anpassen der Matrizen auf geforderte Groe�en
for i=1:SensorOffsetSize-OhneRaeder
    %For Schleife mit der Gre der tatschlichen Sensor Anzahl 8 Eintrge
    %sind bereits belget (4 Reifen, 4 Kreuzpunkte)
    angles=zeros(3,1);
    SensorNummer = SensorOffset(i+OhneRaeder,2);
    SensorMatrix7 = fcn_PunktAusgabeClean(SensorNummer,CleanInputTrans);
    %Bereinigen der Punkte
    
    for j=1:3
        SensorMatrix3(j,:) = [SensorMatrix7(j,2),SensorMatrix7(j,3),SensorMatrix7(j,4)];
    end
    %Nur die ersten 3 (0-2) marker Punkte sind fr die Berechnung noetig
    
    
    %%%%%%%%%%%%%%%%########################Erstellung Abbildung#############################
    SensorMatrixDrehung = SensorMatrix3;
    CGOffset = SensorOffset(i+OhneRaeder,4:6);
    RawOffset = CGOffset;
    SENSOffset = SensorOffset(i+OhneRaeder,7:9);
    
    MitteRichtungPunkt = (SensorMatrixDrehung(2,:)+SensorMatrixDrehung(3,:))*0.5; % Mittelpunkt vom Marker
    SensorRichtung = SensorMatrixDrehung(1,:)-MitteRichtungPunkt;
    SensorQuer = SensorMatrixDrehung(3,:)-SensorMatrixDrehung(2,:);
    SensorTief = -cross(SensorRichtung,SensorQuer);
    
    %Erstellen Marker Koordinatensystem
    %        x
    %        ^
    %        0
    %        |
    % y< 1---|---2
    SensorXUnNorm = SensorMatrixDrehung(1,:)-MitteRichtungPunkt;
    SensorYUnNorm = SensorMatrixDrehung(2,:)-MitteRichtungPunkt;
    SensorZUnNorm = cross(SensorXUnNorm,SensorYUnNorm); %Markernormale im Fahrzeugkoordinatensystem
    
    %WinkelBerechnung
    
    %Yaw
    if SensorZUnNorm(2)>0 %y positiv, Vektor zeigt nach links im Fahrzeugkoordinatensystem
        angles(1)=acosd([SensorZUnNorm(1),SensorZUnNorm(2),0]/norm([SensorZUnNorm(1),SensorZUnNorm(2),0])*[1;0;0]);
    else %y negativ, Vektor zeigt nach rechts im Fahrzeugkoordinatensystem
        angles(1)=-acosd([SensorZUnNorm(1),SensorZUnNorm(2),0]/norm([SensorZUnNorm(1),SensorZUnNorm(2),0])*[1;0;0]);
    end
    
    %Ruecktransformation um Yaw
    Pitchvector=[cosd(-angles(1)),-sind(-angles(1)),0;
        sind(-angles(1)),cosd(-angles(1)),0;
        0,0,1]*SensorZUnNorm'; % Drehung um Z Achse
    
    %Pitch
    if Pitchvector(3)>0 %z positiv, Vektor zeigt nach oben im Fahrzeugkoordinatensystem
        angles(2)=-acosd([Pitchvector(1),0,Pitchvector(3)]/norm([Pitchvector(1),0,Pitchvector(3)])*[1;0;0]);
    else %z negativ, Vektor zeigt nach unten im Fahrzeugkoordinatensystem
        angles(2)=acosd([Pitchvector(1),0,Pitchvector(3)]/norm([Pitchvector(1),0,Pitchvector(3)])*[1;0;0]);
    end
    
    %Ruecktransformation um Yaw und Pitch
    Rollvector = [cosd(-angles(2)),0,sind(-angles(2));
        0,1,0;
        -sind(-angles(2)),0,cosd(-angles(2))]...
        *[cosd(-angles(1)),-sind(-angles(1)),0;
        sind(-angles(1)),cosd(-angles(1)),0;
        0,0,1]...
        *SensorXUnNorm'; %Drehung erfolgt erst um Z, dann um Y
    
    
    %Roll
    if Rollvector(2)>0 %y positiv, Vektor zeigt nach rechts im Fahrzeugkoordinatensystem
        angles(3)=-acosd([Rollvector(1),Rollvector(2),Rollvector(3)]/norm([0,Rollvector(2),Rollvector(3)])*[0;0;1]);
    else %y negativ, Vektor zeigt nach links im Fahrzeugkoordinatensystem
        angles(3)=acosd([Rollvector(1),Rollvector(2),Rollvector(3)]/norm([0,Rollvector(2),Rollvector(3)])*[0;0;1]);
    end
    
    SensorX = SensorXUnNorm/norm(SensorXUnNorm);
    SensorY = SensorYUnNorm/norm(SensorYUnNorm);
    SensorZ = SensorZUnNorm/norm(SensorZUnNorm);
    
    Abb = [SensorX;SensorY;SensorZ]';
    
    PosCG = Abb*CGOffset';
    PosSens = Abb*SENSOffset';
    
    %% Translation
    
    CGPos = PosCG'+SensorMatrix3(1,:);
    SENSPos = PosSens'+SensorMatrix3(1,:);
    SensorMatrixTrans(i+2,:) = [SensorMatrix7(1,1)/10,CGPos(1,1),CGPos(1,2),CGPos(1,3),SENSPos(1,1),SENSPos(1,2),SENSPos(1,3),angles(1),angles(2),angles(3)];
    
end

%Ausgabe der Hinteren R�der um Radstand zu bekommen
SensorNummer = SensorOffset(4,2);
SensorMatrix7 = fcn_PunktAusgabeClean(SensorNummer,CleanInputTrans);
%Bereinigen der Punkte

for j=1:3
    SensorMatrix3(j,:) = [SensorMatrix7(j,2),SensorMatrix7(j,3),SensorMatrix7(j,4)];
end

%% Ausgabe
%Es wird eine Xx7 Matrix erstellt die dann ausgegeben werden kann

%%Winkelberechnung
% for alle sensoren
% n=cross(punkt1-Mittelpunkt,punkt2-MP)
% alpha=arccos(n/norm(n)*x)
% beta=arccos(n/norm(n)*y)
% gamma=arccos(n/norm(n)*z)
% end

end %function