function [centaury_ROT] = fcn_centaury(Matrix)
centaury_ang = acosd(abs(Matrix(2,2))/(sqrt(Matrix(2,2)^2+Matrix(2,3)^2)));


if Matrix(2,2) > 0
    
    if Matrix(2,3) > 0
        quadrant = 'A'
        %     centaury_ROT = -centaury_ROT;
        centaury_ang
        centaury_ROT= [1,0,0;0,cosd(centaury_ang),-sind(centaury_ang);0,sind(centaury_ang),cosd(centaury_ang)]';
        
    else
        quadrant = 'C'
        %         centaury_ROT = centaury_ROT;
        centaury_ang
        centaury_ROT= [1,0,0;0,cosd(centaury_ang),-sind(centaury_ang);0,sind(centaury_ang),cosd(centaury_ang)];
    end
else
    if Matrix(2,3) > 0
        quadrant = 'B'
        centaury_ang = (180) - centaury_ang
        centaury_ROT= [1,0,0;0,cosd(centaury_ang),-sind(centaury_ang);0,sind(centaury_ang),cosd(centaury_ang)]';
    else
        quadrant = 'D'
        centaury_ang = (180) - centaury_ang
        centaury_ROT= [1,0,0;0,cosd(centaury_ang),-sind(centaury_ang);0,sind(centaury_ang),cosd(centaury_ang)];
    end
end

end