function[Rueckgabe] = fcn_EinlesenVermessungMasterPfad()

[Name, Pfad] = uigetfile('.xls','Waehlen Sie eine Datenbank aus');

if any(Name) ~= 0 && any(Pfad) ~= 0
    Rueckgabe = [Pfad, Name];
else
    %Wenn die Benutzer uigetfile abbrechen
    Rueckgabe = 0;
end

end