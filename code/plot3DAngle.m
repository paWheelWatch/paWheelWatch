function [vE] = plot3DAngle(roll, pitch, yaw, x, y, z)
global Roll Pitch Yaw
Roll = roll;
Pitch = pitch;
Yaw = yaw;
fv = stlread('2011 VW Passat .stl');
% workaround fuer das passat modell
fmidx =  (max(fv.vertices(:,1)) + min(fv.vertices(:,1))) / 2;
fmidy =  (max(fv.vertices(:,2)) + min(fv.vertices(:,2))) / 2;
fmidz =  (max(fv.vertices(:,3)) + min(fv.vertices(:,3))) / 2;
for i=1:size(fv.vertices, 1)
    if fv.vertices(i,3) <= 0
        fv.vertices(i,:) = [fmidx fmidy fmidz];
    end
end
vRPY = genRotMat('z',yaw) * genRotMat('y',pitch) * [5; 0; 0];
[az1,el1]=view(vRPY);
% scaling = wheelbase / length, see passat-*-dimensions.pdf
scaling = 1.71;
fv.vertices = fv.vertices - [fmidx 0.9*scaling*fmidy fmidz];
Rz = genRotMat('z',90);
tempvertices = Rz * fv.vertices';
fv.vertices = tempvertices';
vDest = [0.0011 * x;0.0012 * y;0.001 * z];
fv.vertices = fv.vertices - vDest';

patch(fv,'FaceColor',[0.8 0.8 1.0],'EdgeColor','none','FaceLighting','gouraud','AmbientStrength',0.15);
camlight('headlight');
alpha(0.8);
material('dull');
axis('image');
%axis ij;
% gen coordinate system with x y z
origin = [0 0 0];
xaxis = [5 0 0];
yaxis = [0 2 0];
zaxis = [0 0 2];
hold on
plotLine(origin, xaxis);
p(1) = plot3(xaxis(1),xaxis(2),xaxis(3),'>');
plotLine(origin, yaxis);
p(2) = plot3(yaxis(1),yaxis(2),yaxis(3),'>');
plotLine(origin, zaxis);
p(3) = plot3(zaxis(1),zaxis(2),zaxis(3),'^');
%legend([p(1) p(2) p(3)],'x', 'y', 'z');
%axis([-1 2    -1 2    -1 2])
grid on
view(132, 24);
% animation
an1 = animatedline('Marker', '*');
an2 = animatedline('Color', 'r');
iend = 100;
jend = 100;
vS = [5;0;0];
view(132,24);
for i=1:90
    view(132-(42/90)*i,24-(24/90)*i);
    pause(0.01);
end
mytitle = ['roll: ' num2str(roll) '�'];
title(mytitle);
% roll animation
theta = (1 / (iend*10)) * (roll);
Rx = genRotMat('x',theta);
[vRoll, Rp] = drawRot(Rx, vS, iend*10, jend, an1, an2, 0, eye(3),0,0);
mytitle = ['pitch: ' num2str(pitch) '�'];
title(mytitle);
% pitch animation
[az0,el0]=view;
for i=1:90
    view(az0+((az1-az0)/60)*(i-1),el0+((el1-el0)/60)*(i-1));
    pause(0.01);
end
theta = (1 / iend) * (pitch);
Ry = genRotMat('y',theta);
[vRollPitch,Rp] = drawRot(Ry, vRoll, iend, jend, an1, an2, 0, Rp,0,0);
mytitle = ['yaw: ' num2str(yaw) '�'];
title(mytitle);
% yaw animation
theta = (1 / iend) * (yaw);
Rz = genRotMat('z',theta);
[vRollPitchYaw,Rp] = drawRot(Rz, vRollPitch, iend, jend, an1, an2, 0, Rp,pitch,0);
mytitle = 'translation';
title(mytitle);
% translation
% for i=1:90
%     view(125+(7/90)*i,-10+(34/90)*i);
%     pause(0.01);
% end
% view(132,24);
clearpoints(an1);
clearpoints(an2);
vE = drawTran(vRollPitchYaw, 0.1, Rp,pitch,yaw);
mytitle = 'done';
title(mytitle);
hold off
end