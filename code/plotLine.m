function [] = plotLine(p1,p2)
% start point, end point
x = [p1(1) p2(1)];
y = [p1(2) p2(2)];
z = [p1(3) p2(3)];
line(x,y,z);
end

