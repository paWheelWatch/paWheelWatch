%% Einleitung
%Dies ist die Einleitung


%% Einlesen des Raw Inputs
%msgbox('Auswahl')

waitfor(warndlg('Waehlen Sie einen DATENSATZ aus!','Bitte Waehlen!'))
RawInput = fcn_EinlesenDaten();

waitfor(warndlg('Waehlen Sie eine VERMESSUNGMASTER.XLS aus!','Bitte Waehlen!'))

VermessungMasterPfad = fcn_EinlesenVermessungMasterPfad();
LoadingBar = waitbar(0,'Bitte Warten...');

%Hier den Namen der xlsDaten editieren
% %Ausgabe eines beliebigen Punktes###formal(Punkt,Input)

%% Mittel Punkte werden ausgelesen und gespeichert

NullPunkte = fcn_MittelPunkte(RawInput);
waitbar(0.1);
%fcn_Plot3DPoints(NullPunkte);%+++Angucken der RawInput Punkte+++
%^--- Auskommentieren falls ein 3D Plot der Punkte gewuenscht ist

%% Bereinigen des RAW Inputs net

AllePunkteClean = fcn_Bereinigen(RawInput);
waitbar(0.2);
%Bereinigen des RawInputs

%fcn_Plot3DPoints(AllePunkteClean);
%^--- Auskommentieren falls ein 3D Plot der Punkte gewuenscht ist. Kontrolle
%der bereinigten Punkte moeglich

%% Transformation

NullPunkteTrans = fcn_Transformation(NullPunkte, VermessungMasterPfad);
waitbar(0.3);
%Zunaechst Transformation der NullPunkte damit Koordinatensystem erstellt
%werden kann. Transformation ist Translatorisch alsauch Rotatorisch.
%Siehe auch Beschreibung fcn_Transformation
%
%Anmerkung: zur Verringerung des Speicherbedarfs koennen die Punkte
%evtl. ueberschrieben werden. NullPunkteTrans = NullPunkte

AllePunkteCleanTrans = fcn_Transformation(AllePunkteClean, VermessungMasterPfad);
waitbar(0.4);
%Transformation aller Bereinigten Punkte. Genau wie die Transformation der
%NullPunkte um danach eine uebersicht erstellen zu koennen

fcn_Plot3DPoints(NullPunkteTrans);
waitbar(0.5);


%PLot aller NullPunkte damit eine uebersicht angeguckt werden kann.
%Hilfreich um Fehler im Messgitter oder des Koordinatensystems zu erkennen

%% Sensoren Offset einlesen

VermessungMaster = fcn_Einlesen(VermessungMasterPfad);
waitbar(0.6);
%Aufruf der VermessungsMaster.xls um die Informationen welche Sensoren mit
%welchem Offset genutzt werden zu erfassen

Sensoren = fcn_SensorenTrans(VermessungMaster, AllePunkteCleanTrans);
waitbar(0.7);
%Erstellen einer Sensormatrix die die Sensoren mit den bereits eingelesenen
%und transformierten Daten verknuepft

%fcn_Plot3DPoints(Sensoren);
%^--- Auskommentieren falls ein 3D Plot der Punkte gewuenscht ist

%% Sensorenpunkte Ausgabe

fcn_ExcelAusgabe(Sensoren, VermessungMasterPfad);
waitbar(0.8);
%Ausgabe in Excel VermessungsMaster.xls

%% Visuelle Darstellung der Senoren an schamtischem Fahrzeug
fcn_PlotSensorTop(Sensoren);
waitbar(0.9);
%Sensoren mit Auto Bild Top Plot
%evtl. Unterdruecken der Bild Ausgabe

fcn_PlotSensorRear(Sensoren);

%Sensoren mit Auto Bild Rear Plot
%evtl Unterdruecken der Bild Ausgabe
waitbar(1);
close(LoadingBar)