function[Marker] = fcn_PunktAusgabeClean (Number,CleanInput, varargin)
%% Initialisierung
%#############NUR CLEAN INPUT####################
%#############NUR CLEAN INPUT####################
%#############NUR CLEAN INPUT####################
%Augabe [Marker,X,Y,Z]
%Routine zum erkennen wenn wert nicht vorhanden ist eingefuegt

Intern = Number * 10;
j = 1;
MarkerPos = 0;
Marker=zeros(7,4);
sizeRawInput = size(CleanInput);
sizeY=sizeRawInput(1,1);
%Groesse der Schleife bestimmen und Speicher reservieren

%% suche des Punktes
for i=0:sizeY(1,1)
    if CleanInput(j,1) == Intern
        MarkerPos = j;
        i = sizeY(1,1);
        %Marker gefunden und unter MarkerPos gespeichert
        %i mit Abschlussbedingung gesetzt um Schleife zu beenden
    else
        %Durchgehen der einzelnen Eintraege ob Marker vorhanden
        %Wenn nicht wird der Interne Zaehler vergroessert
        
        if j==sizeY(1,1)
            
            
            return;
            %Abbruch wenn Wert nicht vorhanden
            %Verhindert out of range um fehlerfrei durchzulaufen
            
        end
        
        j = j+1;
    end
    
    
end
%entweder Rueckgabe [LEER] oder die Marker Position

%% Ausfuellen der Rueckgabe mit MarkerPos Marker
if nargin == 3
    xEnd = 1;
else
    xEnd = 7;
end
for x=1:xEnd
    Marker(x,:) = [CleanInput(MarkerPos,1), CleanInput(MarkerPos,2), CleanInput(MarkerPos,3), CleanInput(MarkerPos,4)];
    MarkerPos = MarkerPos+1;
end
%Markerposition ist die gesuchte Marker Nummer
%Wird dann mit den gesuchten Koordinaten gefuellt

end