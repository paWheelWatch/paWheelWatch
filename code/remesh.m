function [fvNew] = remesh(fv,dimNo,op,num)
%remesh under certain rules, e.g. remesh(fv,2,'<',0) extracts the y < 0 part 
if ismember(op, ['=', '<', '<=', '>', '>=']) && ismember(dimNo, [1 2 3])
    s = strcat('if fv.vertices(i,dimNo)', op, 'num',...
        ' vert(c,:)=fv.vertices(i,:);',...        
        ' nto(c)=i;',...
        ' c=c+1;',...
        ' end');
else
    warning('undefined operator');
    return
end
c=1;
for i=1:size(fv.vertices,1)
    eval(s);
end
otn=zeros(1,size(fv.vertices,1));
for i=1:c - 1
    otn(nto(i))=i;
end
c=1;
skip = false;
for j=1:size(fv.faces,1)
    for k=1:3
        if ~ismember(fv.faces(j,k), nto)
            skip = true;
            break
        end
    end
    if ~skip
        fa(c,:)=otn(fv.faces(j,:));
        c=c+1;
        %this is crazy, can be use to determine wheel???
        %skip = false;
    end
    skip = false;
end
fvNew.faces = fa;
fvNew.vertices = vert;
end

