function [R] = genRotMat(axis,angle)
% rotation axis, rotation angle in degree
if axis == 'x'
    R = [ 1 0 0;
        0 cosd(angle) -sind(angle);
        0 sind(angle) cosd(angle)];
elseif axis == 'y'
    R = [ cosd(angle) 0 sind(angle);
        0 1 0;
        -sind(angle) 0 cosd(angle)];
elseif axis == 'z'
    R = [ cosd(angle) -sind(angle) 0;
        sind(angle) cosd(angle) 0;
        0 0 1];
else
    return
end
end

