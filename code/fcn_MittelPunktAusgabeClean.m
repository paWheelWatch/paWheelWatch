%#############NUR CLEAN INPUT####################
%#############NUR CLEAN INPUT####################
%#############NUR CLEAN INPUT####################
function[Marker] = fcn_MittelPunktAusgabeClean (Number,CleanInput)
%Return [Marker,X,Y,Z]
%routine zum erkennen wenn wert nicht vorhanden ist eingefuegt
Intern = Number * 10;
j = 1;
MarkerPos = 0;
Marker=zeros(1,4);
sizeRawInput = size(CleanInput);
sizeY=sizeRawInput(1,1);


for i=0:sizeY(1,1)
    
    if CleanInput(j,1) == Intern
        MarkerPos = j;
        i = sizeY(1,1);
        
    else
        %     j = j+1;
        if j==sizeY(1,1)
            
            return;%Abbruch wenn Wert nicht vorhanden
            
        end
        j = j+1;
    end
    
    
end

Marker(1,:) = [CleanInput(MarkerPos,1), CleanInput(MarkerPos,2), CleanInput(MarkerPos,3), CleanInput(MarkerPos,4)];



end