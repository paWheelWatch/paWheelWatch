function [FL,FR,RL,RR] = fcn_sub_BasisKoordinatenSystem (Input, VermessungMasterPfad)
%% Initialisierung
% Abfrage welche Marker auf den Reifen Kleben und Rueckgabe der Koordinaten.

%% Einlesen

RawReifen = fcn_Einlesen(VermessungMasterPfad);
%Einlesen aus der VermessungMaster.xls

RawFL = fcn_PunktAusgabeClean(RawReifen(1,2),Input, true);
RawFR = fcn_PunktAusgabeClean(RawReifen(2,2),Input, true);
RawRL = fcn_PunktAusgabeClean(RawReifen(3,2),Input, true);
RawRR = fcn_PunktAusgabeClean(RawReifen(4,2),Input, true);
%Punkte werden rausgesucht.

%% Bereinigen
%Reihenfolge ist wichtig ReifenPunkte!!!! [RR,RL,FR,FL]
%################################
%Hier aendern wenn an die Reifenschablonen festgelegt sind
FL = [RawFL(1,2),RawFL(1,3),RawFL(1,4)];
FR = [RawFR(1,2),RawFR(1,3),RawFR(1,4)];
RL = [RawRL(1,2),RawRL(1,3),RawRL(1,4)];
RR = [RawRR(1,2),RawRR(1,3),RawRR(1,4)];
%Oben aendern wenn die Reifenschablonen festgelegt sind
%###############################
%Punkte werden ohne die Marker Nummer zurueck gegeben.
%Laesst sich so einfacher verarbeiten.

end