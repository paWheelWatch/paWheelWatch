function [] = fcn_ExcelAusgabe (SensorenInput, VermessungMasterPfad)
%% Initialisierung
%Schreibt die Sensor Punkte in die Output Seite des Excel Sheets

%% Einlesen
OldOutput = xlsread(VermessungMasterPfad,'Output');
SizeOldOutput = size(OldOutput,1);
% einlesen des VermessungMaster.xls

%% Pruefen
%Ist der vermessungsMaster.xls output leer oder muss er ueberschreiben
%werden

if SizeOldOutput == 0
    %pruefen ob alte Excel Seiten leer sind
    
else

    %sonst überschreiben der alten Daten mit Nullen

    OldOutput = nan(SizeOldOutput+1,10);
xlswrite(VermessungMasterPfad,OldOutput,'Output','A2');
%Dann erst schreiben der neuen Daten damit keine flaschen Daten von anderen
%Messungen überbleiben

end

% firstColumn={'Sensornummer','CGx','CGy','CGz','SensorX','SensorY','SensorZ'};
% xlswrite(VermessungMasterPfad,firstColumn,'Output','A1');
xlswrite(VermessungMasterPfad,SensorenInput,'Output','A2');
%Schreiben der Daten falls VermessungMaster.xls Output leer ist

end