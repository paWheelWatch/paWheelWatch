function [TransRotRotRotTransPunkte] = fcn_Transformation (Input, VermessungMasterPfad)
%%Initialisierung
%Input sind Rohdaten die zwingend die Radmittelpunkte und das Aicon
%Koordinatenkreuz enthalten muss. Die Input Daten werden dann zwei mal
%translatorisch (am Anfang und am Ende) und 3 mal rotatorisch bewegt.
%Dann sind alle Punkte im richtigen Koordinatensystem zur weiterverwendung.

%Schreibweise: X, Y, Z f�r Fahrzeugkoordinatensystem und x, y, z f�r
%Ursprungskoordinatensystem (Referenzmarker)

[FL,FR,RL,RR] = fcn_sub_BasisKoordinatenSystem(Input, VermessungMasterPfad);
%Funktion sucht die Radmittelpunkte raus und legt sie in einer Rad matrix
%ab, sodass immer wieder drauf zugegriffen werden kann.
%Pruefen ob alle Radmarker erfasst wurden
if RR == 0
    warndlg('Das Programm findet den Marker (RR) nicht! Starten Sie die Aufnahme erneut.');
    return
end
if RL == 0
    warndlg('Das Programm findet den Marker (RL) nicht! Starten Sie die Aufnahme erneut.');
    return
end
if FR == 0
    warndlg('Das Programm findet den Marker (FR) nicht! Starten Sie die Aufnahme erneut.');
    return
end
if FL == 0
    warndlg('Das Programm findet den Marker (FL) nicht! Starten Sie die Aufnahme erneut.');
    return
end
MitteVA = ((FL+FR)*0.5);
MitteHA = ((RR+RL)*0.5);
%Berechnung der Mittelpunkte zwischen den beiden Vorder und Hinter- Reifen

%%Speicherzuweisung
PositionLastPointALL = size(Input);
AnzahlPunkte = PositionLastPointALL(1,1);
TransPunkte = zeros(AnzahlPunkte,4);
RotTransPunkte = zeros(AnzahlPunkte,4);
RotRotTransPunkte = zeros(AnzahlPunkte,4);
RotRotRotTransPunkte = zeros(AnzahlPunkte,4);
TransRotRotRotTransPunkte = zeros(AnzahlPunkte,4);
%Alle genutzten Matrizen werden vorher mit der richtigen Groesse zugewiesen,
%um spaeter eine schnellere Berechnung zu ermoeglichen

% %% Translation in die Mitte der VA
% for i=1:AnzahlPunkte
%     TransPunkte(i,:) = [Input(i,1),Input(i,2)-MitteVA(1,1),Input(i,3)-MitteVA(1,2),Input(i,4)-MitteVA(1,3)];
% end
% %Transformation ReifenPunkte in die Mitte der VA

%% Translation in die Mitte der HA
for i=1:AnzahlPunkte
    TransPunkte(i,:) = [Input(i,1),Input(i,2)-MitteHA(1,1),Input(i,3)-MitteHA(1,2),Input(i,4)-MitteHA(1,3)];
end
%Transformation ReifenPunkte in die Mitte der HA


%%  Rotation um Fahrzeug Z Achse / Ursprungs y Achse

[FL,FR,RL,RR] = fcn_sub_BasisKoordinatenSystem(TransPunkte, VermessungMasterPfad);
%neuBerechnung der Radmittelpunkte um die erste Rotation zu berechnen

MitteVA = ((FL+FR)*0.5);
MitteHA = ((RR+RL)*0.5);
%Neuberechnung der Mittelpunkte VA und HA

x1 = [1,0,0]';
y1 = [0,1,0]';
z1 = [0,0,1]';

% alpha ist der Winkel f�r die Drehung um die Fahrzeug Z Achse, also nur
% auf der X-Y Ebene vom Fahrzeug
% alpha = acosd((abs(MitteHA(1))*x1(1))/(sqrt((MitteHA(1)^2+MitteHA(3)^2))))
% projektion erstmal auf die x-z Ebene, dann den Winkel berechnen, deswegen
% nur MitteHA(1) und MitteHA(3)
alpha = acosd((abs(MitteVA(1))*x1(1))/(sqrt((MitteVA(1)^2+MitteVA(3)^2))));

%alpha = asind((abs(MitteHA(3))*z1(3))/(norm(MitteHA,2)))
%Berechnung Winkel zwischen Vector MitteHA und X Normalen

%Definition der Quadranten, wobei gegen Uhrzeigersinn positiv ist
%           ^ z(Y)
%    III    |    II 
%------------------------> x(X)
%    IV     |    I
if FL(1) > FR(1)
    if FL(3) > FR(3)
        phi = alpha;    %X Koordinate befindet sich im II. Quadranten!
        Quadrant = 2;
    else
        phi = 180 - alpha; %X Koordinate befindet sich im III. Quadranten!
        Quadrant = 3;
    end
else
    if FL(3) > FR(3)
        phi = -alpha; %X Koordinate befindet sich im I. Quadranten
        Quadrant = 1;
    else
        phi = 180 + alpha; %X Koordinate befindet sich im IV. Quadranten
        Quadrant = 4;
    end
end

printphi = phi;
%Wichtige Unterscheidung um die Richtung der Drehung festzulegen.
%Wird so gewaehlt, sodass die Fahrzeug Y Achse poitiv nach links in
%Fahrzeugrichtung zeigt

%Rotationsmatrix um Fahrzeug Z
RotZ = [cosd(phi)   0  sind(phi);
           0        1         0 ;
        -sind(phi)  0  cosd(phi)];


for j=1:AnzahlPunkte
    RotTransPunkte(j,:) = [TransPunkte(j,1),...
        TransPunkte(j,2)*RotZ(1,1)+TransPunkte(j,3)*RotZ(2,1)+TransPunkte(j,4)*RotZ(3,1),...
        TransPunkte(j,2)*RotZ(1,2)+TransPunkte(j,3)*RotZ(2,2)+TransPunkte(j,4)*RotZ(3,2),...
        TransPunkte(j,2)*RotZ(1,3)+TransPunkte(j,3)*RotZ(2,3)+TransPunkte(j,4)*RotZ(3,3)];
end
%Rotation wird in neuer Matrix abgespeicher um Fehler zu vermeiden

%% Rotation um Fahrzeug X Achse

[FL,FR,RL,RR] = fcn_sub_BasisKoordinatenSystem(RotTransPunkte, VermessungMasterPfad);
%neu Berechnung der Radmittelpunkte um die zweite Rotation zu berechnen

y1 = [0,1,0]';
% beta = acosd((abs(FL(2))*y1(2))/(sqrt((FL(2)^2+FL(3)^2))))
beta = acosd((abs(RL(2))*y1(2))/(sqrt((RL(2)^2+RL(3)^2))));
%beta = asind((FL(3)*z1(3))/norm(FL,2));

if FL(2)>FR(2)
    beta =  beta;%Y befindet sich im III. Quadranten!
    Quadrant = 1;
else
    Quadrant = 2;
    beta = 180-beta; %Y befindet sich im III Quadranten!
end

printbeta = beta;

RotX = [1    0                 0;
        0 cosd(beta) -sind(beta);
        0 sind(beta) cosd(beta)];
%berechnung Winkel zwischen Vector FL und Y Normalen
%Bestimmung des Winkels um den gedreht wird

for k=1:AnzahlPunkte
    RotRotTransPunkte(k,:) = [RotTransPunkte(k,1),...
        RotTransPunkte(k,2)*RotX(1,1)+RotTransPunkte(k,3)*RotX(2,1)+RotTransPunkte(k,4)*RotX(3,1),...
        RotTransPunkte(k,2)*RotX(1,2)+RotTransPunkte(k,3)*RotX(2,2)+RotTransPunkte(k,4)*RotX(3,2),...
        RotTransPunkte(k,2)*RotX(1,3)+RotTransPunkte(k,3)*RotX(2,3)+RotTransPunkte(k,4)*RotX(3,3)];
end
%Rotation wird in neuer Matrix abgespeicher um Fehler zu vermeiden


%% Rotation um Fahrzeug Y Achse
[FL,FR,RL,RR] = fcn_sub_BasisKoordinatenSystem(RotRotTransPunkte, VermessungMasterPfad);
%neu Berechnung der Radmittelpunkte um die zweite Rotation zu berechnen

MitteHA = ((RR+RL)*0.5);
MitteVA = ((FL+FR)*0.5);
%Neuberechnung der Mitte HA wichtig da nun darauf referenziert wird.


% gamma = atand(abs(MitteHA(3))/abs(MitteHA(1)));
gamma = atand(abs(MitteVA(3))/abs(MitteVA(1)));


% vorher war hier MitteHA(3) < MitteVA(3)
if MitteVA(3) > MitteHA(3)
    Quadrant = 2;           %Z befindet sich im II. Quadranten
    gamma = -gamma;         
end

printgamma = gamma;
%berechnung Winkel zwischen X und Z Komponente Mitte HA
%Der Winkel ist negativ da der Punkt hinter der VA und damit im negativen
%liegt.

% RotY = [cosd(gamma), -sind(gamma), 0; sind(gamma), cosd(gamma), 0; 0,0,1];

RotY = [cosd(gamma) 0  sind(gamma);
            0       1           0 ;
       -sind(gamma) 0 cosd(gamma)];


%Bestimmung des Winkels um den gedreht wird

for h=1:AnzahlPunkte
    RotRotRotTransPunkte(h,:) = [RotRotTransPunkte(h,1),...
        RotRotTransPunkte(h,2)*RotY(1,1)+RotRotTransPunkte(h,3)*RotY(2,1)+RotRotTransPunkte(h,4)*RotY(3,1),...
        RotRotTransPunkte(h,2)*RotY(1,2)+RotRotTransPunkte(h,3)*RotY(2,2)+RotRotTransPunkte(h,4)*RotY(3,2),...
        RotRotTransPunkte(h,2)*RotY(1,3)+RotRotTransPunkte(h,3)*RotY(2,3)+RotRotTransPunkte(h,4)*RotY(3,3)];
end
%Rotation wird in neuer Matrix abgespeicher um Fehler zu vermeiden


%% Translation um Kreuz tiefe
RawKreuz = fcn_Einlesen(VermessungMasterPfad); %Einlesen der Kreuz Punkte
%Einlesen der KreuzPunkte

RawNullFL = fcn_MittelPunktAusgabeClean(RawKreuz(5,2),RotRotRotTransPunkte);
RawNullFR = fcn_MittelPunktAusgabeClean(RawKreuz(6,2),RotRotRotTransPunkte);
RawNullRL = fcn_MittelPunktAusgabeClean(RawKreuz(7,2),RotRotRotTransPunkte);
RawNullRR = fcn_MittelPunktAusgabeClean(RawKreuz(8,2),RotRotRotTransPunkte);
%KreuzPunkte werden bereinigt

RawSum = 1/4 * (abs(RawNullFL(4))+abs(RawNullFR(4))+abs(RawNullRL(4))+abs(RawNullRR(4)));
%Mittelung der Punkte um einen Absolutwert fuer die Translation zu erhalten

for t=1:AnzahlPunkte
    TransRotRotRotTransPunkte(t,:) = [RotRotRotTransPunkte(t,1),...
        RotRotRotTransPunkte(t,2),...
        RotRotRotTransPunkte(t,3),...
        RotRotRotTransPunkte(t,4)+RawSum];
end
%Rotation wird in neuer Matrix abgespeicher um Fehler zu vermeiden

end