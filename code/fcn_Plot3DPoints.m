%% 3DPlot der Input Punkte
function [fig] = fcn_Plot3DPoints (RawInput)
%Ausgabe ist eine Figure mit den Input Argumenten als Plot in 3D

%% Initialisierung
fig = figure('Name','Punkte', 'visible','off');
Punkte = RawInput;
sizeAllePunkte = size(Punkte);
hold on
% Hold on damit alle Punkte aufgenommen werden

%% Plot Schleife
for x=1:sizeAllePunkte
    plot3(Punkte(x,2),Punkte(x,3),Punkte(x,4), '*');
end


hold off

end