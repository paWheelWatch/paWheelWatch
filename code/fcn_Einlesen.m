function[RawInput] = fcn_Einlesen(DateiName)

%% Funktionsbeschreibung
%Einlesen der Daten aus dem xls mit Namen DateiName####

%% Code

RawInput = xlsread(DateiName);

%Uebergebener Dateiname (bereits String) wird mittels xlsread Kommando importiert.
%Rueckgabe ist eine Matrix mit den Argumenten aus dem Excel sheet.
%Ueberschriften und Tabellenbenennung werden ignoriert.
%Eintraege die keine Zahl sind werden mit n.a. ausgefiltert
end
