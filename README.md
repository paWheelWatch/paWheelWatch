[Link to WheelWatch!](https://www.aicon3d.com/de-DE/products/vehicle-testing/wheelwatch)
![Image of WheelWatch](./WheelWatch.jpg)

# Entwicklung einer Messmethodik zur extrinsischen Kalibrierung von Sensoren auf Basis der vorangegangenen Arbeit mit dem WheelWatch System
- [x] Anpassung des Koordinatenursprungs inkl. Fehlertoleranz betrachten beim Aufspannen des Fahrzeugkoordinatensystems
- [x] Ermittlung der Winkellage der Sensoren
- [x] Methode zur verdreh-sicheren Anbringung der Marker auf den Sensoren (bereits angesprochene Adapterkonstruktionen)
- [x] Eingabemaske in Form einer GUI bei Programmstart
- [x] Formatierte Darstellung Messergebnisse und ggf. Überführung in ein Messprotokoll in Word- oder Excel-Form
- [x] Dokumentation und Anleitung/Prozessbeschreibung
